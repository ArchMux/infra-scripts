CPN_SKIP_TESTS+=(
    # Fails some tests
    # Last checked: 02 May 2020 (version 2.9.10-r1)
    # Context:
    # ## XML regression tests
    # File ./test/icu_parse_test.xml generated an error
    # ## XML regression tests on memory
    # File ./test/icu_parse_test.xml generated an error
    # ## XML entity subst regression tests
    # File ./test/icu_parse_test.xml generated an error
    #
    # [ more icu_parse_test.xml related errors ]
    #
    # Total 3174 tests, 11 errors, 0 leaks
    dev-libs/libxml2

    # Fails a test
    # Last checked: 02 May 2020 (version 2.4.48)
    # Context:
    # [13] $ setfattr -n user -v value f -- failed
    # setfattr: f: Not supported                                               != setfattr: f: Operation not supported
    #
    # Problem:
    # musl stringifies errno ENOTSUP differently than glibc and attr's test
    # hardcodes that error string
    sys-apps/attr

    # Fails some tests
    # Last checked: 03 May 2020 (version: 5.1.0)
    # Context:
    # 3 TESTS FAILED
    # _clos1way6: (wrong output order)
    # _nonfatal3:
    #   nonfatal3.awk:4: warning: remote host and port information (localhost, 0) invalid: System error
    # _testext:
    #   ! test_errno() returned 1, ERRNO = No child processes
    #   vs.
    #   ! test_errno() returned 1, ERRNO = No child process
    sys-apps/gawk

    # Fails the locale test
    # Last checked: 19 Sep 2021 (version: 5.34.0-r1)
    # Context:
    # Test Summary Report
    # -------------------
    # ../lib/locale.t (Wstat: 0 Tests: 682 Failed: 1)
    #   Failed test:  450
    dev-lang/perl

    # Fails a test
    # Last checked: 19 Sep 2021 (version 2.37.2)
    # Context:
    # col: multibyte input: [02] invalid                 ... FAILED (col/multibyte-invalid)
    sys-apps/util-linux

    # Fails some tests
    # Last checked: 19 Sep 2021 (version 0.21-r1)
    # Context:
    # FAIL: msgconv-2
    # FAIL: msgmerge-compendium-6
    # FAIL: xgettext-python-3
    sys-devel/gettext

    # Fails a test
    # last checked: 19 Sep 2021 (version: 10.37)
    # Context:
    # FAIL: RunGrepTest
    #
    #  @@ -1,22 +1,22 @@
    #  Arg1: [T] [he ] [ ] Arg2: |T| () () (0)
    # +The quick brown
    #  Arg1: [T] [his] [s] Arg2: |T| () () (0)
    #  Arg1: [T] [his] [s] Arg2: |T| () () (0)
    #  Arg1: [T] [he ] [ ] Arg2: |T| () () (0)
    #  Arg1: [T] [he ] [ ] Arg2: |T| () () (0)
    #  Arg1: [T] [he ] [ ] Arg2: |T| () () (0)
    # -The quick brown
    #  [...]
    dev-libs/pcre2

    # Fails a test
    # Last checked: 26 Apr 2022 (version: 1.4.19)
    # Context:
    # The test started failing with musl 1.2.3
    # Upstream bug: https://lists.gnu.org/archive/html/bug-gnulib/2022-04/msg00013.html
    sys-devel/m4

    # Fails a bunch of tests
    # Last checked: 10 Jun 2022 (version: 23.5)
    # Context:
    # Running ./killall.test/killall.exp ...
    # FAIL: killall using signal HUP
    # FAIL: killall using signal SIGHUP
    # FAIL: killall using signal SIGINT
    # FAIL: killall using signal QUIT
    # FAIL: killall using signal SIGQUIT
    # FAIL: killall using signal SIGILL
    # FAIL: killall using signal TRAP
    # FAIL: killall using signal SIGTRAP
    # [...]
    #
    # Upstream bug: https://gitlab.com/psmisc/psmisc/-/issues/18
    sys-process/psmisc

    # Fails a test
    # Last checked: 16 Aug 2022 (version: 1.12.1)
    # Context:
    # 98% tests passed, 1 tests failed out of 45
    #
    # The following tests FAILED:
    #          10 - googletest-port-test (Failed)
    #
    dev-cpp/gtest
)
