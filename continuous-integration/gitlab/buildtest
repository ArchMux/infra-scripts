#!/bin/bash

# Print message with current timestamp
_einfo() {
    echo -e "[$(date)] ${@}" 1>&2
}

# Verbose edo variant that always prints the output of the command
_edov() {
    _einfo "# ${@}"
    "${@}"
}

# Quiet variant of edo to keep the log short
#
# This function will print the command to be executed and nothing else if that
# command succeeds. If the command fails however, its complete output will be
# printed for debugging
_edo() {
    echo -e "[$(date)] # ${@}" 1>&2

    local rc
    local logfile=$(mktemp)
    ( "${@}" &> ${logfile} ) || rc=$?

    if [[ ${rc} -ne 0 ]]; then
        echo "Command failed with exit code ${rc}"
        echo ""
        echo "Output log:"
        echo "%<-----"
        cat ${logfile}
        echo ">%-----"

        exit ${rc}
    fi

    rm ${logfile}
}

# Finds missing repositories in the resolution of a package
find_missing_repositories() {
    cave resolve --hide '*/*::pbin' ${1} \
        | grep -oE '::unavailable(-unofficial)? \(in ::[A-Za-z0-9+_.-]*\)' \
        | sed -e 's#::unavailable.* (in ::\([^)]\+\))#repository/\1#g' \
        | sort | uniq
}

set -e

chgrp paludisbuild /dev/tty

# In case the repo URL and the repo name differ, e.g:
# URL = gitlab.exherbo.org/user/user-exheres ($REPO here)
# Name = user ($PALUDIS_REPO here)
PALUDIS_REPO=${PALUDIS_REPO:=${REPO}}

echo "Repository URL: ${CI_REPOSITORY_URL}"
echo "Branch: ${CI_COMMIT_REF_NAME}"
echo "Commit SHA: ${CI_COMMIT_SHA}"
echo ""

# Enable our repo
_edo cave sync
_edo cave resolve -x -Ks repository/${PALUDIS_REPO}
# Call generate metadata to speed things up later
_edo cave generate-metadata

rm -Rf /tmp/ciwork
mkdir /tmp/ciwork
pushd /tmp/ciwork >/dev/null

_edo git clone -b ${CI_COMMIT_REF_NAME} ${CI_REPOSITORY_URL} ${REPO}
pushd ${REPO} >/dev/null

# Make sure that we always test all commits between the current HEAD and the official master.
# For the master itself we have to figure out which commits to build in a different way.
# Old documentation for CI_BUILD_BEFORE_SHA says that it specifies
# "The first commit that were included in push request".
# But this is actually not true. In some cases it is the last commit before the push,
# sometimes it is invalid and sometimes it is HEAD.
# Just build the last commit if it is HEAD and hope that it is the right thing to do.
PREV_REV="${CI_BUILD_BEFORE_SHA}"
_einfo "Previous SHA according to Gitlab: ${PREV_REV}"
if [[ ${PREV_REV} == "HEAD" ]]; then
    PREV_REV="${CI_COMMIT_SHA}^"
fi
if [[ ${CI_COMMIT_REF_NAME} != "master" ]] || [[ ${CI_PROJECT_NAMESPACE} != ${REPO_NAMESPACE} ]]; then
    _edo git remote add official https://gitlab.exherbo.org/${REPO_NAMESPACE}/${REPO}.git
    _edo git fetch official
    PREV_REV=remotes/official/master
fi

# Cut the changes in the master branch that are not part of this branch.
# We are not interested in them.
PREV_REV=$(git merge-base ${CI_COMMIT_SHA} ${PREV_REV})
_einfo "Previous SHA that will be used: ${PREV_REV}"

# Add fork as local sync source and sync to the commit that we want to test
sed -e "/^sync/ s|$| local: git+file:///tmp/ciwork/${REPO}|" \
    -i /etc/paludis/repositories/${PALUDIS_REPO}.conf
_edo cave sync ${PALUDIS_REPO} -s local -r ${CI_COMMIT_SHA}

declare -a PKGS UNTESTED_CHANGES

declare -a \
    MODIFIED_GLOBAL_EXLIBS \
    MODIFIED_LOCAL_EXLIBS \
    MODIFIED_PACKAGES

changes=$(git diff-tree --no-commit-id --diff-filter=d --name-only -r ${PREV_REV} ${CI_COMMIT_SHA})
while read line; do
    if [[ "${line}" =~ ^exlibs/([^/]+)\.exlib$ ]]; then
        MODIFIED_GLOBAL_EXLIBS+=( ${line} )
    elif [[ "${line}" =~ ^packages/([^/]+)/([^/]+)/([^/]+)\.exlib$ ]]; then
        MODIFIED_LOCAL_EXLIBS+=( ${line} )
    elif [[ "${line}" =~ ^packages/[^/]+/[^/]+/([^/]+)\.exheres-0$ ]]; then
        MODIFIED_PACKAGES+=( ${line} )
    else
        UNTESTED_CHANGES+=( "${line}" )
    fi
done <<< "${changes}"

popd >/dev/null
popd >/dev/null


echo ""
echo "Detected modifications:"
echo "  Global exlibs:"
for exlib in "${MODIFIED_GLOBAL_EXLIBS[@]}"; do
    echo "    - ${exlib}"
done

echo "  Local exlibs:"
for exlib in "${MODIFIED_LOCAL_EXLIBS[@]}"; do
    echo "    - ${exlib}"
done

echo "  Packages:"
for pkg in "${MODIFIED_PACKAGES[@]}"; do
    echo "    - ${pkg}"
done

if [[ -n "${MODIFIED_GLOBAL_EXLIBS[@]}" ]]; then
    # If we haven't already, enable all repos that have ours as a master according to ::unavailable
    # so we have a sufficient number of exlib consumers to test with
    dependent_repos=$(cave print-ids -m "repository/*[.dependencies<repository/${PALUDIS_REPO}]" -f '%c/%p\n')
    if [[ -n ${dependent_repos} ]]; then
        _edo cave resolve -x -Ks ${dependent_repos}

        # Call generate metadata to speed things up later
        _edo cave generate-metadata
    fi
fi

echo ""
_einfo "Figuring out packages to test..."

# Handle global and local exlibs
for line in "${MODIFIED_GLOBAL_EXLIBS[@]}" "${MODIFIED_LOCAL_EXLIBS[@]}"; do
    c='*' p='*' exlib= pnv=

    if [[ "${line}" =~ ^packages/([^/]+)/([^/]+)/([^/]+)\.exlib$ ]]; then
        c=${BASH_REMATCH[1]}
        [[ ${BASH_REMATCH[2]} != exlibs ]] && p=${BASH_REMATCH[2]}
        exlib=${BASH_REMATCH[3]}
    elif [[ "${line}" =~ ^exlibs/([^/]+)\.exlib$ ]]; then
        exlib=${BASH_REMATCH[1]}
    else
        continue
    fi

    exlib_pkgs=()
    # Our exlibs can only be used in repos that have us as a master.
    for r in ${PALUDIS_REPO} $(cave print-ids -m "repository/*[.dependencies<repository/${PALUDIS_REPO}]" -f '%p\n'); do
        # Need to build new-style spec for use in config files below
        exlib_pkgs+=( $(cave print-ids -s install -m "${c}/${p}::${r}[.INHERITED<${exlib}]" -f '%c/%p%:%s::%r[=%V]\n') )
    done

    if [[ ${#exlib_pkgs[@]} -gt 50 ]]; then
        _einfo "exlib ${exlib} has many consumers, picking 50 to test at random"
        PKGS+=( $(shuf -n50 -e "${exlib_pkgs[@]}" ) )
    elif [[ ${#exlib_pkgs[@]} -gt 0 ]]; then
        PKGS+=( "${exlib_pkgs[@]}" )
    else
        _einfo "Couldn't find any exheres that require ${exlib}.exlib"
        UNTESTED_CHANGES+=( "${line}" )
    fi
done

for line in "${MODIFIED_PACKAGES[@]}"; do
    [[ "${line}" =~ ^packages/[^/]+/[^/]+/([^/]+)\.exheres-0$ ]] || continue
    pnv=${BASH_REMATCH[1]}

    # Need to build new-style spec for use in config files below
    exheres_pkgs=( $(cave print-ids -s install -m "=${pnv}::${PALUDIS_REPO}" -f '%c/%p%:%s::%r[=%V]\n') )

    if [[ ${#exheres_pkgs[@]} -gt 0 ]]; then
        PKGS+=( "${exheres_pkgs[@]}" )
    else
        _einfo "Couldn't find any exheres that match =${pnv}::${PALUDIS_REPO}"
        UNTESTED_CHANGES+=( "${line}" )
    fi
done

# Get rid of duplicates.
IFS=$'\n' PKGS=( $(sort <<<"${PKGS[*]}" | uniq) )
unset IFS

if [[ ${#PKGS[@]} -eq 0 ]]; then
    _einfo "No package to build. No exlib recognised. Don't know what to do. Exiting."
    exit 0
fi

echo ""
echo "These are the packages that I will build:"
for pkg in "${PKGS[@]}"; do
    echo " - ${pkg}"
done
echo ""

# Set up pbins
pushd /pbins >/dev/null
mkdir -p {dist,pbin{,/metadata,/packages,/profiles}}
echo 'pbin' > pbin/profiles/repo_name
echo 'masters = arbor' > pbin/metadata/layout.conf
touch pbin/profiles/options.conf
touch pbin/metadata/categories.conf
chown -R paludisbuild:paludisbuild .
chmod -R g+w .
popd >/dev/null

cat <<EOF > /etc/paludis/repositories/pbin.conf
format = e
location = /pbins/pbin
distdir = /pbins/dist
binary_distdir = /pbins/dist
binary_destination = true
binary_keywords_filter = amd64 ~amd64
tool_prefix = x86_64-pc-linux-gnu-
importance = -10
EOF

# Only build pbins if this is the master branch in one of our repositories
declare -a VIA_BIN
if [[ ${CI_COMMIT_REF_NAME} == "master" ]]; then
    if [[ ${CI_PROJECT_NAMESPACE} == "exherbo" || \
         ${CI_PROJECT_NAMESPACE} == "exherbo-unofficial" || \
         ${CI_PROJECT_NAMESPACE} == "exherbo-devs" ]]; then
        VIA_BIN=("--via-binary" "*/*" "--one-binary-per-slot")
    fi
fi

# Only enable tests for the target
mkdir -p /etc/paludis/{options.conf.d,package_mask.conf.d}
echo '*/* build_options: -recommended_tests' > /etc/paludis/options.conf.d/no_tests.conf

# We are explicitely handling errors from here on
set +e
declare -a FAILED_PKGS SUCCEEDED_PKGS MSCAN_RESULTS CHECK_SLOTS_RESULTS
for PKG in "${PKGS[@]}"; do
    echo "**************************************************************"
    echo "Building package ${PKG}"
    echo "**************************************************************"

    # ensure that tests run for the target and that it isn't installed from an existing pbin
    # interestingly, this still allows --via-binary to work for the target, which is exactly what we want!
    # NOTE(moben): shouldn't technically be needed as we include the repo in the ${PKG} spec, but better be safe
    echo "${PKG} build_options: recommended_tests" > /etc/paludis/options.conf.d/target_tests.conf
    echo "${PKG/::*\[/::pbin\[}" > /etc/paludis/package_mask.conf.d/target_pbin.conf

    # Install all repositories for dependencies in ::unavailable.
    # This is a bit hacky but works for now.
    # --hide ::pbin to ensure that we install the origin repos in case they contain users/groups we need
    declare -a NEW_REPOS
    while
        NEW_REPOS=( $(find_missing_repositories ${PKG}) )
        [[ -n "${NEW_REPOS[@]}" ]]
    do
        _edo cave resolve -Ks "${NEW_REPOS[@]}" -x
    done
    # Call generate metadata to speed things up later
    _edo cave generate-metadata

    # Handle confirmations automatically
    # NOTE(moben): --execute-resolution-program true should also suppress the "unread news" hook but doesn't in the container?
    TMPFILE=$(mktemp -uq)
    cave resolve "${VIA_BIN[@]}" ${PKG} --execute-resolution-program true \
        --display-resolution-program "cave print-resolution-required-confirmations" > ${TMPFILE}
    ARGS=$(handle_confirmations < ${TMPFILE})
    rc=$?
    if [[ $rc -gt 0 ]]; then
        echo "***** I FAILED! ***********************************************"
        cat ${TMPFILE}
        echo "*************** COMMITTING SUICIDE NOW! ***********************"
        FAILED_PKGS+=( ${PKG} )
        continue
    fi

    # Perform the build itself
    echo "ARGS: ${ARGS}"
    echo "**************************************************************"
    opts=(
        "${VIA_BIN[@]}"
        ${ARGS}
        --lazy
    )
    # Check if resolution with binary promotion works
    _edov cave resolve "${opts[@]}" --promote-binaries if-same ${PKG}
    rc=$?
    if [[ ${rc} -gt 0 ]]; then
        _einfo "Resolving with binary promotion failed! Will do build without..."
    else
        _einfo "Resolving with binary promotion successful! Building with binaries..."
        opts+=( --promote-binaries if-same )
    fi

    _edov cave resolve "${opts[@]}" ${PKG} -x
    rc=$?
    if [[ ${rc} -gt 0 ]]; then
        _einfo "Build failed!"
        FAILED_PKGS+=( ${PKG} )
        continue
    fi

    SUCCEEDED_PKGS+=( ${PKG} )

    _einfo "**************************************************************"
    _einfo "Running QA tools on ${PKG}"

    # Check dependencies
    _einfo "Inspect ${PKG} with mscan2.rb"
    MSCAN_RESULTS+=( "$(
        echo "**************************************************************"
        echo "Dependencies I believe to have found for ${PKG} (excluding system):"
        /usr/local/bin/mscan2.rb -i system --hide-libs unused ${PKG/::*\[/\[} 2>&1
    )" )

    _einfo "Inspect ${PKG} with check_slots"
    CHECK_SLOTS_RESULTS+=( "$(
        echo "**************************************************************"
        echo "Check if dependencies are slotted properly for ${PKG}:"
        /usr/local/bin/check_slots "${PKG}" 2>&1
    )" )
done

# Get rid of now unused pbin tarballs
# (also any distfiles we fetched but there shouldn't be a lot of those if pbins are working correctly)
# only delete files older than 1h to avoid races between one job creating a pbin tarball
# and another deleting it before the pbin exheres could be written
if [[ -n "${VIA_BIN[@]}" ]]; then
  _edo cave print-unused-distfiles -i pbin | xargs -I{files} find {files} -not -mmin -60 -exec rm {} +
fi

if [[ ${#MSCAN_RESULTS[@]} -gt 0 ]]; then
    echo "**************************************************************"
    echo "mscan2.rb results: ${MSCAN_RESULTS[@]/#/$'\n'}"
    echo "**************************************************************"
fi
if [[ ${#CHECK_SLOTS_RESULTS[@]} -gt 0 ]]; then
    echo "**************************************************************"
    echo "check_slots results: ${CHECK_SLOTS_RESULTS[@]/#/$'\n'}"
    echo "**************************************************************"
fi
if [[ ${#SUCCEEDED_PKGS[@]} -gt 0 ]]; then
    echo "Builds that succeeded: ${SUCCEEDED_PKGS[@]/#/$'\n'    }"
    echo "**************************************************************"
fi
if [[ ${#FAILED_PKGS[@]} -gt 0 ]]; then
    echo "Builds that failed: ${FAILED_PKGS[@]/#/$'\n'    }"
    echo "**************************************************************"
fi
if [[ ${#UNTESTED_CHANGES[@]} -gt 0 ]]; then
    echo "I was unable to find packages to test changes to these files: ${UNTESTED_CHANGES[@]/#/$'\n'    }"
    echo "**************************************************************"
fi

[[ ${#FAILED_PKGS[@]} -gt 0 ]] && exit 1 || exit 0

