#!/bin/bash

if [[ -z "$1" ]] ; then
    echo "USAGE: $0 stage_archive"
    exit 1
fi

ARCHIVE=$1
NAME=$(basename "${ARCHIVE}")
NAME=${NAME%.tar.xz}
DATE=$(date +%Y%m%d)

DOCKER_IMAGE_NAME=${NAME}-base
DOCKER_IMAGE_TAG=${DATE}
DOCKER_IMAGE=exherbo/${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_TAG}
DOCKER_IMAGE_LATEST=exherbo/${DOCKER_IMAGE_NAME}:latest

set -e

echo "Importing base container..."
docker import "${ARCHIVE}" ${DOCKER_IMAGE}
docker push ${DOCKER_IMAGE}

docker tag ${DOCKER_IMAGE} ${DOCKER_IMAGE_LATEST}
docker push ${DOCKER_IMAGE_LATEST}

if [[ ${NAME} == "exherbo-x86_64-pc-linux-gnu" ]] ; then
    echo "Building CI container..."
    pushd "$(dirname "${BASH_SOURCE[0]}")"/ci
    docker build --no-cache --pull -t exherbo/exherbo_ci .
    docker push exherbo/exherbo_ci
    popd

    echo "Building DID container..."
    pushd "$(dirname "${BASH_SOURCE[0]}")"/did
    docker build --no-cache --pull -t exherbo/exherbo_did .
    docker push exherbo/exherbo_did
    popd
fi

